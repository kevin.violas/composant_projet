# Composant project

### Après le git clone et l'installation de couchdb, il faut installer les composants :
```
bower install bower.json --save
```

### Ensuite, on lance le serveur (pour windows) :
```
python -m http.server
```

### Pour linux :
```
python3 -m http.server
```

### L'application tourne ensuite à l'URL suivante :
```
localhost:8000
```

### L'application dispose des fontionnalités suivantes :
* Voir la liste des livres enregistrés dans la base
* Consulter les informations d'un livre, on peut ensuite sur cette même page :
  * Modifier les informations par exemple le prix
  * Supprimer le livre
  * Ces deux actions feront recharger la page pour montrer que les modifications ont été appliquées
* Voir la liste des livres d'un auteur en cliquant sur son nom. Cela ouvrira une pop-up avec la liste de ses livres et les informations relatives
